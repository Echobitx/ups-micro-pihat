# UPS micro piHAT

The Uninterrupted Power Supply (UPS Micro PiHAT) attaches to the Raspberry Pi Zero W, and intelligently supplies power to it, so that it
it remains uninterrupted during power interruptions.